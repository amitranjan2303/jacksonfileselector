package developer.amit.com.jacksonfileselector.utils;

import developer.amit.com.jacksonfileselector.R;

public class Constants {
    private static boolean MULTIPLE_SELECTION = false;

    private static int CUSTOM_THEME = R.style.LibAppTheme;

    public static boolean isMultipleSelection() {
        return MULTIPLE_SELECTION;
    }

    public static void setMultipleSelection(boolean multipleSelection) {
        MULTIPLE_SELECTION = multipleSelection;
    }

    public static int getCustomTheme() {
        return CUSTOM_THEME;
    }

    public static void setCustomTheme(int customTheme) {
        CUSTOM_THEME = customTheme;
    }
}
